package com.seis770;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        String characters =  "ABCD";

        Random generator = new Random();

        Map<String,Stock> stockList = new HashMap<String,Stock>();

        EventManager eventManager = EventManager.getUniqueInstance();

        eventManager.register(StockBroker.getUniqueInstance(), "stockCreated");

        eventManager.register(StockMonitor.getUniqueInstance(), "stockCreated");

        eventManager.register(StockMonitor.getUniqueInstance(), "stockStatusChanged");

        boolean running = true;

        int totalTimeToRun = 0;
        while(running) {
            try {
                Thread.sleep(2000);
                totalTimeToRun += 2000;
                StringBuilder sb = new StringBuilder();
                for(int charCounter = 0; charCounter < 3; charCounter++) {
                    sb.append(characters.charAt(generator.nextInt(characters.length()-1)));
                }
                if(stockList.containsKey(sb.toString())) {
                    // update the stock status
                    Stock stock = (Stock) stockList.get(sb.toString());
                    double price;
                    if((Math.random() * 2) == 1) {
                        price = stock.getCurrentStockStatus().getPrice().getPrice() + generator.nextDouble()*10;
                    } else {
                        price = stock.getCurrentStockStatus().getPrice().getPrice() - generator.nextDouble()*10;
                        if(price < 0) {
                            price = 0;
                        }
                    }
                    Money money = new Money(price,"usd");
                    stock.addStockStatus(new StockStatus(new DateTime(),money));
                } else {
                    // add a new stock
                    stockList.put(sb.toString(),Stock.createStock(sb.toString(),new StockStatus(new DateTime(),new Money(generator.nextDouble()*100,"usd")),eventManager));

                }

                if(totalTimeToRun == 1000 * 60 *  2) {
                    running = false;
                }

            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

}
