package com.seis770;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/21/13
 * Time: 6:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class Money {

    private String symbol;
    private double price;

    public Money(double price, String symbol) {
        this.price = price;
        this.symbol = symbol;
    }

    public String getSymbol() {
        return this.symbol;
    }

    public double getPrice() {
        return this.price;
    }

}
