package com.seis770;

import java.util.List;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/21/13
 * Time: 6:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class Stock {

    String stockSymbol;
    List<StockStatus> stockStatusList = new ArrayList<StockStatus>();
    EventManager eventManager;

    public Stock(String stockSymbol, StockStatus stockStatus, EventManager eventManager) {
        this.stockSymbol = stockSymbol;
        this.stockStatusList.add(stockStatus);
        this.eventManager = eventManager;
        this.inform("stockCreated");
    }

    public static Stock createStock(String stockSymbol, StockStatus stockStatus, EventManager eventManager) {
        return new Stock(stockSymbol, stockStatus, eventManager);
    }

    public Stock(String stockSymbol) {
        this.stockSymbol = stockSymbol;
    }

    public String getStockSymbol() {
        return this.stockSymbol;
    }

    public StockStatus getCurrentStockStatus() {
        return this.stockStatusList.get(this.stockStatusList.size()-1);
    }

    public void addStockStatus(StockStatus stockStatus) {
        this.stockStatusList.add(stockStatus);
        this.inform("stockStatusChanged");
    }

    public void inform(String event) {
        this.eventManager.notify(this,event);
    }


}
