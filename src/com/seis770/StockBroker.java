package com.seis770;

import javax.swing.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/21/13
 * Time: 6:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class StockBroker implements Subscriber {

    private static StockBroker uniqueInstance;

    protected StockBroker() {

    }

    public static StockBroker getUniqueInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new StockBroker();
        }
        return uniqueInstance;
    }

    @Override
    public void update(ArrayList<Stock> stockArrayList) {
        System.out.println(String.format("Stock created: %s", stockArrayList.get(stockArrayList.size()-1).getStockSymbol()));
    }
}
