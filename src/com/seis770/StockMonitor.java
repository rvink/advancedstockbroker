package com.seis770;

import java.awt.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/24/13
 * Time: 6:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class StockMonitor implements Subscriber {


    private static StockMonitor uniqueInstance = null;

    public static StockMonitor getUniqueInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new StockMonitor();
        }
        return uniqueInstance;
    }

    protected StockMonitor() {

    }

    @Override
    public void update(ArrayList<Stock> stockArrayList) {
        Stock stock = stockArrayList.get(stockArrayList.size()-1);
        StockStatus stockStatus = stock.getCurrentStockStatus();
        Money money = stockStatus.getPrice();
        System.out.println(String.format("Stock Status: %s - %s - %1.2s", stock.getStockSymbol(), money.getSymbol(), money.getPrice()));
    }
}
