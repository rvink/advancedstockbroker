package com.seis770;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/21/13
 * Time: 8:54 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Subscriber {

    public void update(ArrayList<Stock> stockArrayList);

}
