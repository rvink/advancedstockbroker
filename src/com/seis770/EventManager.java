package com.seis770;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/28/13
 * Time: 8:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class EventManager {

    public static EventManager uniqueInstance = null;

    List<Stock> stockList = new ArrayList<Stock>();

    public Map eventMap;

    public static EventManager getUniqueInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new EventManager();
        }
        return uniqueInstance;
    }

    protected EventManager() {
        this.eventMap = new HashMap<Subscriber, HashSet<String>>();
    }

    public void register(Subscriber subscriber, String event) {

        if (!this.eventMap.containsKey(subscriber)) {
            this.eventMap.put(subscriber, new HashSet<String>() {});
        }

        Set<String> stringSet = (HashSet) this.eventMap.get(subscriber);

        stringSet.add(event);
        this.eventMap.put(subscriber,stringSet);

    }

    public void notify(Stock stock, String event) {
        this.addStock(stock);
        Map<Subscriber,HashSet<String>> map = this.eventMap;
        for(Map.Entry<Subscriber,HashSet<String>> entry : map.entrySet()) {
            Subscriber mappedEntry = (Subscriber) entry.getKey();
            for(String mappedEvent : (HashSet<String>) entry.getValue()) {
                if(mappedEvent.equals(event)) {
                    mappedEntry.update((ArrayList<Stock>) this.stockList);
                }
            }
        }
    }

    private void addStock(Stock stock) {
        boolean add = false;
        if (this.stockList.size() == 0) {
            add = true;
        } else {
            for (Stock stockItem : this.stockList) {
                if (!stockItem.getStockSymbol().equals(stock.getStockSymbol())) {
                    add = true;
                }
            }
        }
        if (add) {
            this.stockList.add(stock);
        }
    }
}
